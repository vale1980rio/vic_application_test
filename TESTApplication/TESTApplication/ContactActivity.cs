﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using TESTApplication.Utility;
using static Android.Provider.ContactsContract.CommonDataKinds;

namespace TESTApplication
{
    [Activity(Label = "ContactActivity")]
    public class ContactActivity : Activity
    {
        private ImageView image;
        private TextView descrizione;
        private ListView numbers;

        public string uri_str = "";

        private List<string> numbers_by_contact = new List<string>();
        Button btn;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Contact);
            btn = FindViewById<Button>(Resource.Id.buttoncerca);
           
            descrizione = FindViewById<TextView>(Resource.Id.textViewDescrizione);
            image = FindViewById<ImageView>(Resource.Id.imageViewContact);

       


            btn.Click += Btn_Click;

        }


       

        private void Btn_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(Intent.ActionPick);
            intent.SetData(ContactsContract.Contacts.ContentUri);
            StartActivityForResult(intent, 17);

        }

        private void Btn_ClicCercaContatto(object sender, EventArgs e)
        {

        }




        


        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case (17):

                    string numero = "";
                    int count_numeri = 0;
                    if (resultCode == Result.Ok)
                    {

                        Android.Net.Uri contactData = data.Data;
                        var c = ContentResolver.Query(contactData, null, null, null, null);
                        if (c.MoveToFirst())
                        {
                            descrizione.Text = c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.DisplayName));


                            String contactId = c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id));
                            long contactId_long = long.Parse(c.GetString(c.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id)));

                            var contactUri = ContentUris.WithAppendedId(ContactsContract.Contacts.ContentUri, contactId_long);
                            var contactPhotoUri = Android.Net.Uri.WithAppendedPath(contactUri, Contacts.Photos.ContentDirectory);
                            image.SetImageURI(contactPhotoUri);
                            uri_str = contactPhotoUri.ToString();

                            if (image.Drawable == null)
                            {
                                image.SetImageResource(Resource.Drawable.ContactImage);
                            }
                            var uri = ContactsContract.Contacts.ContentUri;
                            string[] projection = { Phone.Number, CommonColumns.Type };

                            var cursor = ContentResolver.Query(Phone.ContentUri, projection, ContactsContract.RawContactsColumns.ContactId + "=" + contactId, null, null);
                            count_numeri = cursor.Count;
                            if (cursor != null)
                            {
                                if (count_numeri > 0)
                                {
                                    while (cursor.MoveToNext())
                                    {
                                        numbers_by_contact.Add(cursor.GetString(0).Replace(" ", ""));

                                    }

                                    numbers = FindViewById<ListView>(Resource.Id.listViewContacts);
                                    ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Resource.Layout.list_item, numbers_by_contact);
                                    numbers.Adapter = adapter;
                                    numbers.ItemClick += Numbers_ItemClick;  
                                }
                            }
                            c.Close();
                            cursor.Close();

                            //Toast.MakeText(this, name + " - Trovati " + count_numeri + " Numeri \n" + numero, ToastLength.Long).Show();
                        }

                    }


                    break;
            }

        }

        private void Numbers_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var  num = numbers.GetItemAtPosition(e.Position).ToString();
            Sqlite sqlite = new Sqlite();
            //sqlite.Insert_PNumbers(num,uri_str,descrizione.Text);
            ToastColor.YellowToastColor("Numero inserito Correttamente",this.ApplicationContext);
            this.FinishAndRemoveTask();
        }

        private void Numbers_Click(object sender, EventArgs e)
        {
            
        }

    }
}