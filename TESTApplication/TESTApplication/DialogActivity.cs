﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Refractored.Controls;
using static Android.App.ActivityManager;


namespace TESTApplication
{

    [Activity(Theme = "@style/AppTheme", Label = "Allarme", NoHistory = true, LaunchMode = Android.Content.PM.LaunchMode.SingleInstance, ScreenOrientation = ScreenOrientation.Portrait)]

    public class DialogActivity : Activity
    {

        private BroadcastMessageCatcher mReceiver;
        string chiamate = "";




        Vibrator vibrator = (Vibrator)Application.Context.GetSystemService(Context.VibratorService);

        Button btn;
        Dialog dialog;
        MediaPlayer mPlayer;

        string photo_uri = string.Empty;
        string missed = string.Empty;
        string number = string.Empty;
        string state = string.Empty;

        public void KillApplication()
        {
            dialog.Cancel();
            this.Finish();
        }


        protected override void OnDestroy()
        {
            StopPlayer();
            base.OnDestroy();
        }


        protected override void OnResume()
        {
            base.OnResume();

            IntentFilter intentFilter = new IntentFilter("android.intent.action.DIALOG_BC");

            mReceiver = new BroadcastMessageCatcher();
            mReceiver.setDialogActivityHandler(this);
            this.RegisterReceiver(mReceiver, intentFilter);
        }


        protected override void OnPause()
        {
            base.OnPause();
            this.UnregisterReceiver(mReceiver);
        }


        protected void ok_click(object sender, EventArgs e)
        {

            Android.Net.Uri uri = Android.Net.Uri.Parse("tel:" + number);
            Intent intent = new Intent(Intent.ActionCall, uri);
            StartActivity(intent);
            KillApplication();
        }

        protected void no_click(object sender, EventArgs e)
        {
            KillApplication();
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Alarm);
            LinearLayout layout = (LinearLayout)FindViewById(Resource.Id.linearLayout1);
            AnimationDrawable drawable = new AnimationDrawable();
            Handler handler = new Handler();

            drawable.AddFrame(new ColorDrawable(Color.White), 300);
            drawable.AddFrame(new ColorDrawable(Color.ParseColor("#7c2a2a")), 300);
            drawable.OneShot = false;


            try
            {
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                layout.SetBackgroundDrawable(drawable);
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
            }
            catch
            {

            }

            Handler h = new Handler();
            Action myAction = () =>
             {
                 drawable.Start();
             };

            h.PostDelayed(myAction, 1000);

            Sqlite sqlite = new Sqlite();

            number = Intent.GetStringExtra("number") ?? string.Empty;
            photo_uri = Intent.GetStringExtra("photoURI") ?? string.Empty;
            chiamate = Intent.GetStringExtra("chiamate") ?? string.Empty;
            string nome = sqlite.Get_Descrizione(number);


            dialog = new Dialog(this);
            dialog.Window.SetDimAmount(0);
            dialog.RequestWindowFeature((int)WindowFeatures.NoTitle); //before 
            dialog.SetContentView(Resource.Layout.Dialog_Header_Custom);
            dialog.SetTitle("Custom Alert Dialog");
            dialog.SetCancelable(false);



            Window.SetGravity(GravityFlags.Top);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn |
               WindowManagerFlags.DismissKeyguard |
               WindowManagerFlags.ShowWhenLocked |
               WindowManagerFlags.TurnScreenOn);


            ImageButton btnCall = (ImageButton)dialog.FindViewById(Resource.Id.call);
            ImageButton btnClose = (ImageButton)dialog.FindViewById(Resource.Id.close);


            CircleImageView image = (CircleImageView)dialog.FindViewById(Resource.Id.imageViewContact);
            TextView utente = (TextView)dialog.FindViewById(Resource.Id.editText);
            TextView numero = (TextView)dialog.FindViewById(Resource.Id.editTextNumber);

            //ESTER EGG
            if (chiamate == "VALE17RIO")
            {
                image.SetImageResource(Resource.Drawable.valerio);
            }
            else if (chiamate == "CLAUDIO")
            {
                image.SetImageResource(Resource.Drawable.claudio);
            }
            else
            {
                image.SetImageURI(Android.Net.Uri.Parse(photo_uri));

                if (image.Drawable == null)
                {
                    image.SetImageResource(Resource.Drawable.ContactImage);
                }
            }

            utente.Text = nome.ToUpper() + " \n TI HA CHIAMATO " + chiamate + " VOLTE NEGLI ULTIMI 5 MINUTI.";
            numero.Text = number;

            //Claudio
            if (chiamate == "SMS")
            {
                utente.Text = nome.ToUpper() + " \nTI HA INVIATO UN VIC MESSAGE";
            }

            //ESTER EGG
            if (chiamate == "VALE17RIO")
            {
                utente.Text = "A Software Developed \n by \n Valerio Maccheroni";
            }
            if (chiamate == "CLAUDIO")
            {

                utente.Text = "A Software Developed\n by \n Claudio Francioni";
            }

            btnCall.Click += ok_click;
            btnClose.Click += no_click;
            //contact_to_delete = test.Contact_ID;

            dialog.Show();




            StartPlayer();

        }



        public void StopPlayer()
        {
            mPlayer.Stop();
            vibrator.Cancel();
        }

        public void ChangeText(string testo)
        {
            TextView messageView = (TextView)dialog.FindViewById(Android.Resource.Id.Message);
            messageView.Text = testo;
        }

        public void StartPlayer()
        {
            if (mPlayer == null)
            {
                mPlayer = new MediaPlayer();
            }
            AudioManager am = (AudioManager)Android.App.Application.Context.GetSystemService(Context.AudioService);

            int maxVol = am.GetStreamMaxVolume(Android.Media.Stream.Music);


            //int maxVol = 1;

            am.SetStreamVolume(Android.Media.Stream.Music, maxVol, VolumeNotificationFlags.PlaySound);

            //NotificationManager mNotificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            //if (mNotificationManager.IsNotificationPolicyAccessGranted)
            //{
            //    am.RingerMode = RingerMode.Normal;
            //}
            //mPlayer.Reset();
            var fd = global::Android.App.Application.Context.Assets.OpenFd("tornado.mp3");
            mPlayer.SetDataSource(fd.FileDescriptor, fd.StartOffset, fd.Length);
            //mPlayer.SetDataSource(filePath);
            mPlayer.Prepare();
            mPlayer.Looping = true;
            mPlayer.Start();
            long[] mVibratePattern = new long[] { 0, 1000, 200, 1000, 400, 1000, 600, 1000, 800, 1000, 800, 1000, 600, 1000, 400, 1000, 200, 1000 };

            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                vibrator.Vibrate(VibrationEffect.CreateWaveform(mVibratePattern, 0));
            }
            else
            {
                try
                {
#pragma warning disable CS0618 // Il tipo o il membro è obsoleto
                    vibrator.Vibrate(mVibratePattern, 0);
#pragma warning restore CS0618 // Il tipo o il membro è obsoleto
                }
                catch
                {

                }
            }

        }


    }

    [BroadcastReceiver]
    [IntentFilter(new[] { "android.intent.action.DIALOG_BC" })]
    public class BroadcastMessageCatcher : BroadcastReceiver
    {
        DialogActivity da = null;

        public void setDialogActivityHandler(DialogActivity main)
        {
            da = main;
        }

        public override void OnReceive(Context context, Intent intent)
        {
            try
            {
                da.KillApplication();
            }
            catch
            {

            }
        }
    }
}