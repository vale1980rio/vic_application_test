﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using SQLite;
using TESTApplication.PersonalDataQueries;

namespace TESTApplication
{
    [Table("PriorityNumbers")]
    public class PriorityNumbers
    {
        public string Numero { get; set; }

        public bool Hooked { get; set; }

        public string photo_uri { get; set; }

        public string Descrizione { get; set; }

        public string Contact_ID { get; set; }

        public int VicLevel { get; set; }
    }

    public class Sqlite
    {
        SQLiteConnection db;

        private void Conn()
        {
            try
            {
                var dbName = "Numbers.db3";
                //string dbPath = Path.Combine("/sdcard/", dbName);

                string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
                db = new SQLiteConnection(dbPath);
            }
            catch
            {

            }
        }

        public int Get_VicLevel(string numero)
        {
            try
            {
                Conn();
                int vic = 0;



                db.CreateTable<PriorityNumbers>();

                vic = db.ExecuteScalar<int>("select VicLevel from PriorityNumbers where numero like '%" + numero + "' limit 1");


                return vic;
            }
            catch { return 100; }
        }

        public string Get_Number(string numero)
        {
            try
            {
                Conn();
                int chiamate = 0;

                string PrioritarioSetted;

                db.CreateTable<PriorityNumbers>();

                PrioritarioSetted = db.ExecuteScalar<string>("select numero from PriorityNumbers where numero like '%" + numero + "'' limit 1");
                if (string.IsNullOrEmpty(PrioritarioSetted))
                {
                    PrioritarioSetted = "NO NUMBER TO CHECK!!";
                }

                return PrioritarioSetted;
            }
            catch { return ""; }
        }


        public string Get_PhotoUriByNumero(string numero)
        {
            try
            {
                Conn();
                int chiamate = 0;

                string PrioritarioSetted;

                db.CreateTable<PriorityNumbers>();

                PrioritarioSetted = db.ExecuteScalar<string>("select photo_uri from PriorityNumbers where numero like '%" + numero + "' limit 1");
                if (string.IsNullOrEmpty(PrioritarioSetted))
                {
                    PrioritarioSetted = "NO NUMBER TO CHECK!!";
                }

                return PrioritarioSetted;
            }
            catch (Exception e) { return e.Message; }
        }

        public string Get_PhotoUri(string numero)
        {
            try
            {
                Conn();
                int chiamate = 0;

                string PrioritarioSetted;

                db.CreateTable<PriorityNumbers>();

                PrioritarioSetted = db.ExecuteScalar<string>(string.Format("select photo_uri from PriorityNumbers where numero like '%{0}' limit 1", numero));
                if (string.IsNullOrEmpty(PrioritarioSetted))
                {
                    PrioritarioSetted = "";
                }

                return PrioritarioSetted;
            }
            catch { return ""; }
        }

        public string Get_Descrizione(string numero)
        {
            try
            {
                Conn();
                int chiamate = 0;

                string PrioritarioSetted;

                db.CreateTable<PriorityNumbers>();

                PrioritarioSetted = db.ExecuteScalar<string>(string.Format("select descrizione from PriorityNumbers where numero like '%{0}' limit 1", numero));
                if (string.IsNullOrEmpty(PrioritarioSetted))
                {
                    PrioritarioSetted = "";
                }

                return PrioritarioSetted;
            }
            catch { return ""; }
        }

        public int Get_Number_Count(string numero)
        {
            try
            {
                Conn();

                int PrioritarioSetted_count;

                db.CreateTable<PriorityNumbers>();

                PrioritarioSetted_count = db.ExecuteScalar<int>("select count(*) from PriorityNumbers where numero like '%" + numero + "'");


                return PrioritarioSetted_count;
            }
            catch { return 0; }
        }

        public void Insert_PNumbers(string Numero, string uri, string descrizione, string Contact_ID, int VicLevel)
        {
            Conn();

            db.CreateTable<PriorityNumbers>();

            descrizione = descrizione.Replace("'", "");

            int numeri_count = db.ExecuteScalar<int>("select count(*) from PriorityNumbers where numero = '" + Numero + "'");
            if (numeri_count == 0)
            {
                db.CreateTable<PriorityNumbers>();
                string query = string.Format("INSERT INTO  PriorityNumbers (numero,photo_uri,descrizione,Contact_ID,VicLevel,hooked) VALUES ('{0}','{1}','{2}','{3}',{4},0) ", Numero, uri, descrizione, Contact_ID, VicLevel);
                db.Query<PriorityNumbers>(query);

            }
        }
        public void Delete_PNumbers(string Contact_ID)
        {
            Conn();
            db.CreateTable<PriorityNumbers>();
            string query = string.Format("delete from PriorityNumbers where Contact_ID = '" + Contact_ID + "'");
            db.Query<PriorityNumbers>(query);
        }

        public void Insert_VIC_Level(string Contact_ID, int VicLevel)
        {
            Conn();

            db.CreateTable<PriorityNumbers>();
            string query = string.Format("UPDATE  PriorityNumbers Set VicLevel = {0} where Contact_ID = '{1}' ", VicLevel, Contact_ID);
            db.Query<PriorityNumbers>(query);
        }

        public void Insert_Numbers_States(string Numero, int State, string State_str)
        {
            Conn();
            db.CreateTable<Call>();
            string query = string.Format("INSERT INTO  Calls (Number,CallType,CallType_str,Date,Date_str) VALUES ('{0}',{1},'{2}',DATETIME('now'),DATETIME('now')) ", Numero, State, State_str);
            db.Query<Call>(query);
        }

        public void Clear_Call()
        {
            Conn();
            db.CreateTable<Call>();
            db.DeleteAll<Call>();
        }

        public List<Call> Get_All_Calls()
        {
            Conn();
            db.CreateTable<Call>();
            return db.Table<Call>().ToList();
        }

        public List<PriorityNumbers> Get_Contacts()
        {
            try
            {
                Conn();

                try
                {
                    Conn();
                    int chiamate = 0;

                    string PrioritarioSetted;

                    db.CreateTable<PriorityNumbers>();

                    List<PriorityNumbers> Contacts = db.Query<PriorityNumbers>("select distinct '' as  Numero , Hooked , photo_uri ,Descrizione , Contact_ID,VicLevel from PriorityNumbers order by Descrizione").ToList();


                    return Contacts;
                }
                catch (Exception r)
                {
                    return null;
                }
            }
            catch { return null; }
        }

        #region HOOK
        public string Get_Hooked()
        {
            Conn();
            int chiamate = 0;

            string PrioritarioSetted;

            db.CreateTable<PriorityNumbers>();

            PrioritarioSetted = db.ExecuteScalar<string>("select Hooked from PriorityNumbers limit 1");
            if (string.IsNullOrEmpty(PrioritarioSetted))
            {
                PrioritarioSetted = "NO NUMBER TO CHECK!!";
            }

            return PrioritarioSetted;
        }

        public void Insert_Hooked()
        {
            Conn();

            db.CreateTable<PriorityNumbers>();

            string query = string.Format("UPDATE PriorityNumbers set Hooked = 0 ");

            db.Query<PriorityNumbers>(query);

        }

        public void Remove_Hooked()
        {
            Conn();

            db.CreateTable<PriorityNumbers>();

            string query = string.Format("UPDATE PriorityNumbers set Hooked = 0 ");

            db.Query<PriorityNumbers>(query);
        }

        #endregion

        public void Cancella_Chiamate_Oltre_1_Giorno()
        {
            try
            {
                Conn();

                db.CreateTable<Call>();
                db.Execute("delete from Calls where Date < (SELECT DATETIME('now', '-1 day'))");
            }
            catch { }
        }


    }
}

