﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Refractored.Controls;

namespace TESTApplication.Utility
{
    public class CustomListAdapter : BaseAdapter<PriorityNumbers>
    {
        Activity context;
        List<PriorityNumbers> list;

        public CustomListAdapter(Activity _context, List<PriorityNumbers> _list)
            : base()
        {
            this.context = _context;
            this.list = _list;
        }

        public override int Count
        {
            get { if (list != null) return list.Count; else return 0; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override PriorityNumbers this[int index]
        {
            get { return list[index]; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;

            // re-use an existing view, if one is available
            // otherwise create a new one
            if (view == null)
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItemRow, parent, false);

            PriorityNumbers item = this[position];
            view.FindViewById<TextView>(Resource.Id.Title).Text = item.Descrizione;

            if ((item.VicLevel == 0))
            {
                view.FindViewById<TextView>(Resource.Id.Description).Text = string.Format("0");
                view.FindViewById<TextView>(Resource.Id.Description).SetTextColor(Color.ParseColor("#707070"));
                view.FindViewById<ImageView>(Resource.Id.VIcImageCall).SetImageResource(Resource.Drawable.alarm_mini);
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.Description).Text = VicConverter.VIC_Chiamate_Converter(item.VicLevel).ToString();
                view.FindViewById<TextView>(Resource.Id.Description).SetTextColor(Color.ParseColor("#7c2a2a"));
                view.FindViewById<ImageView>(Resource.Id.VIcImageCall).SetImageResource(Resource.Drawable.alarm_mini_red);
            }

            var VIcImage = view.FindViewById<ImageView>(Resource.Id.VIcImage);


            if (item.VicLevel == 0)
            { VIcImage.SetImageResource(Resource.Drawable.stop); }
            if (item.VicLevel == 1)
            { VIcImage.SetImageResource(Resource.Drawable.one); }
            if (item.VicLevel == 2)
            { VIcImage.SetImageResource(Resource.Drawable.two); }
            if (item.VicLevel == 3)
            { VIcImage.SetImageResource(Resource.Drawable.three); }

            //if ((item.VicLevel == 2) || (item.VicLevel == 1)) { view.FindViewById<ImageView>(Resource.Id.VIcImageSMS).SetImageResource(Resource.Drawable.message_red); }
            //else { view.FindViewById<ImageView>(Resource.Id.VIcImageSMS).SetImageResource(Resource.Drawable.message); }

            var image = view.FindViewById<CircleImageView>(Resource.Id.Thumbnail);
            image.SetImageURI(Android.Net.Uri.Parse(item.photo_uri));

            if (image.Drawable == null)
            {
                image.SetImageResource(Resource.Drawable.ContactImage);
            }

            return view;
        }
    }

}