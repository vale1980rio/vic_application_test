﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TESTApplication.Utility
{
    public static class VicConverter
    {
        public static int VIC_Chiamate_Converter(int code)
        {
            if (code == 1) return 1;
            else if (code == 2) return 3;
            else if (code == 3) return 5;
            else if (code == 0) return 1000;
            else return 5;
        }

        public static int Next_Vic_Code(int code)
        {
            if (code == 1) return 0;
            else if (code == 2) return 1;
            else if (code == 3) return 2;
            else if (code == 0) return 3;
            else return 3;
        }

        public static void VicItemListViewConverter(View v, PriorityNumbers item)
        {
            if (v == null)
                return;

            var text = v.FindViewById<TextView>(Resource.Id.Description);
            var text_1 = v.FindViewById<TextView>(Resource.Id.Title).Text;
            //testo 
            if ((item.VicLevel == 0))
            {

                text.Text = "0";
                text.SetTextColor(Color.ParseColor("#707070"));
                v.FindViewById<ImageView>(Resource.Id.VIcImageCall).SetImageResource(Resource.Drawable.alarm_mini);
            }
            else
            {
                text.Text = string.Format(VIC_Chiamate_Converter(item.VicLevel).ToString());
                text.SetTextColor(Color.ParseColor("#7c2a2a"));
                v.FindViewById<ImageView>(Resource.Id.VIcImageCall).SetImageResource(Resource.Drawable.alarm_mini_red);
            }

            //Immagini 
            var VIcImage = v.FindViewById<ImageView>(Resource.Id.VIcImage);

            if (item.VicLevel == 0)
            { VIcImage.SetImageResource(Resource.Drawable.stop); }
            if (item.VicLevel == 1)
            { VIcImage.SetImageResource(Resource.Drawable.one); }
            if (item.VicLevel == 2)
            { VIcImage.SetImageResource(Resource.Drawable.two); }
            if (item.VicLevel == 3)
            { VIcImage.SetImageResource(Resource.Drawable.three); }

            //if ((item.VicLevel == 2)|| (item.VicLevel == 1)) { v.FindViewById<ImageView>(Resource.Id.VIcImageSMS).SetImageResource(Resource.Drawable.message_red); }
            //else { v.FindViewById<ImageView>(Resource.Id.VIcImageSMS).SetImageResource(Resource.Drawable.message); }
        }
    }
}